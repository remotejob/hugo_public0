package main

import (
	"time"

	"gitlab.com/remotejob/hugo_public0/apiai"

	"log"

	"strings"

	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
)

func main() {

	router := gin.New()

	router.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     true,
		ValidateHeaders: false,
	}))

	router.POST("/chat", func(c *gin.Context) {
		question := c.PostForm("question")

		log.Println(question)
		record := apiai.GetAnswer("019262def6234213919aec8c58f9618b", "ru", question)

		if strings.HasPrefix(strings.TrimSpace(record.Result.Action), "input.unk") {

			log.Println("try ENGLSH")

			// questionTranslated := google_translate.Service("en", question)

			// record = apiai.GetAnswer("d54b0e6268ca4df0aa5feda28b68dcb6", "en", questionTranslated)

			// _answer := record.Result.Fulfillment.Speech

			// answerTranslated := google_translate.Service("ru", _answer)
		} else {
			log.Println("record.Result.Action", record.Result.Action, len(record.Result.Action))
		}
		answer := record.Result.Fulfillment.Speech
		c.JSON(200, gin.H{

			"answer": answer,
		})
	})

	// By default it serves on :8080 unless a
	// PORT environment variable was defined.
	router.Run()
	// router.Run(":3000") for a hard coded port
}
