package apiai

import (
	"testing"

	uuid "github.com/satori/go.uuid"
)

var _uuid uuid.UUID

func init() {

	_uuid = uuid.NewV4()

}

func TestGetAnswer(t *testing.T) {
	type args struct {
		bearer   string
		uuid     uuid.UUID
		lang     string
		question string
	}
	tests := []struct {
		name string
		args args
		// want domains.QResponse
	}{
		// TODO: Add test cases.
		// {"test0", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Квартира ещё в продаже"}},
		// {"test1", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Могу я сам с вами связаться"}},
		// {"test2", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Мой номер +3584451202801"}},
		// {"test3", args{"d54b0e6268ca4df0aa5feda28b68dcb6", _uuid, "en", "+3584451202801"}},
		// {"test4", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "support@test.com"}},
		// {"test0", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Квартира ещё в продаже"}},
		// // {"test1", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Могу я сам с вами связаться"}},
		// {"test2", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Мой номер +3584451202801"}},
		// {"test4", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "support@test.com"}},
		{"test0", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Квартира ещё в продаже"}},
		// {"test1", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Могу я сам с вами связаться"}},
		// {"test2", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Мой номер +3584451202801"}},
		{"test4", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "Мой емаил support@test.com"}},
		{"test5", args{"019262def6234213919aec8c58f9618b", _uuid, "ru", "OK"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// if got := GetAnswer(tt.args.bearer, tt.args.uuid, tt.args.lang, tt.args.question); !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("GetAnswer() = %v, want %v", got, tt.want)
			// }

			GetAnswer(tt.args.bearer, tt.args.uuid, tt.args.lang, tt.args.question)

		})
	}
}
