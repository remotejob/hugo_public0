package apiai

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/remotejob/hugo_public0/domains"
)

func GetAnswer(bearer string, uuid uuid.UUID, lang string, question string) domains.QResponse {

	_now := time.Now()
	// u1 := uuid.NewV4()

	url := fmt.Sprintf("https://api.api.ai/api/query")
	// log.Println(url)

	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		// return
	}
	q := req.URL.Query()
	q.Add("v", "20150910")
	q.Add("query", question)
	q.Add("lang", lang)
	q.Add("sessionId", uuid.String())
	q.Add("timezone", _now.String())
	req.URL.RawQuery = q.Encode()

	// fmt.Println(req.URL.String())
	// Replace 9ea93023b7274cfbb392b289658cff0b by your Client access token
	req.Header.Add("Authorization", "Bearer "+bearer)

	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)

	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()

	// Fill the record with the data from the JSON
	var record domains.QResponse

	// Use json.Decode for reading streams of JSON data
	if err := json.NewDecoder(resp.Body).Decode(&record); err != nil {
		log.Println(err)
	}

	// fmt.Println("Status = ", record.Status.Code)
	fmt.Println("Response = ", record)

	return record
}
